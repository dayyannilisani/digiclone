from rest_framework import status

SERVER_API_NOT_READY = {
    'status': status.HTTP_404_NOT_FOUND,
    'error': {
        'code': -2,
        'message': 'Api is not ready yet',
        'userMessage': 'متاسفانه هنوز این سرویس آماده استفاده نمی باشد'
    }
}

SERVER_METHOD_NOT_ALLOWED = {
    'status': status.HTTP_405_METHOD_NOT_ALLOWED,
    'error': {
        'code': -1,
        'message': 'method not allowed',
        'userMessage': 'متد فراخوانده شده اشتباه است',
    }
}

SERVER_INTERNAL_ERROR = {
    'status': status.HTTP_500_INTERNAL_SERVER_ERROR,
    'error': {
        'code': 0,
        'message': '',
        'userMessage': 'خطای داخلی سرور'
    }
}

API_INVALID_TOKEN = {
    'status': status.HTTP_401_UNAUTHORIZED,
    'error': {
        'code': 1,
        'message': 'invalid token',
        'userMessage': 'توکن شما معتبر نمی باشد'
    }
}

API_EXPIRED_TOKEN = {
    'status': status.HTTP_401_UNAUTHORIZED,
    'error': {
        'code': 2,
        'message': 'expired token',
        'userMessage': 'توکن شما منقضی شده است'
    }
}

API_LOGIN_REQUIRED = {
    'status': status.HTTP_401_UNAUTHORIZED,
    'error': {
        'code': 3,
        'message': 'login required',
        'userMessage': 'لطفا به سیستم وارد شوید'
    }
}

API_ADMIN_REQUIRED = {
    'status': status.HTTP_401_UNAUTHORIZED,
    'error': {
        'code': 4,
        'message': 'admin required',
        'userMessage': 'متاسفانه شما اجازه چنین عملی را ندارید'
    }
}

API_INVALID_INPUT = {
    'status': status.HTTP_400_BAD_REQUEST,
    'error': {
        'code': 5,
        'message': 'invalid request',
        'userMessage': 'ورودی اشتباه است',
    }
}

API_INVALID_CREDENTIALS = {
    'status': status.HTTP_401_UNAUTHORIZED,
    'error': {
        'code': 6,
        'message': 'wrong credentials',
        'userMessage': "نام کاربری و یا رمز عبور شما اشتباه است"
    }
}

API_USER_NOT_FOUND = {
    'status': status.HTTP_400_BAD_REQUEST,
    'error': {
        'code': 7,
        'message': 'invalid user of token',
        'userMessage': "کاربر مربوط به توکن شما پیدا نشد"
    }
}

API_ITEM_NOT_FOUND = {
    'status': status.HTTP_404_NOT_FOUND,
    'error': {
        'code': 8,
        'message': 'item not found',
        'userMessage': "ایتم مورد نظر شما پیدا نشد"
    }
}