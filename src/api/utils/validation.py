from django.core.exceptions import ValidationError


def validate_phone(value: str):
    if not value.startswith('0'):
        raise ValidationError('شماره تلفن شما باید به صفر شروع شود')
    if not value.isdecimal():
        raise ValidationError('شماره تلفن شما معتبر نمی باشد')
    if not len(value) == 11:
        raise ValidationError('شماره تلفن باید ۱۱ رقم باشد')


def generate_validation_message(field_name, error_types='all'):
    result = {}
    if error_types == 'all':
        result["invalid"] = field_name + " انتخابی شما مناسب نمی باشد"
        result["unique"] = field_name + " انتخابی شما از قبل مورد استفاده قرار گرفته است"
        result["null"] = field_name + " شما نمی تواند خالی باشد"
        result["blank"] = field_name + " شما نمی تواند خالی باشد"
        result["required"] = field_name + " شما نمی تواند خالی باشد"

    else:
        if "invalid" in error_types:
            result["invalid"] = field_name + " انتخابی شما مناسب نمی باشد"
        if "unique" in error_types:
            result["unique"] = field_name + " انتخابی شما از قبل مورد استفاده قرار گرفته است"
        if "null" in error_types:
            result["null"] = field_name + " شما نمی تواند خالی باشد"
        if "blank" in error_types:
            result["blank"] = field_name + " شما نمی تواند خالی باشد"
        if "required" in error_types:
            result["required"] = field_name + " شما نمی تواند خالی باشد"

    return result
