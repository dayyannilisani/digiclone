import random
import string
from operator import itemgetter

from django.db import models
from django.db import transaction

from . import errors
from .my_error import MyError
from ..apps.user.models import MyUser


def get_random_string(length=16):
    letters = string.ascii_lowercase + string.ascii_uppercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def create_item(model, values):
    item = model()
    for key in vars(item):
        if not key.startswith('_') and not key == 'id':
            if key in values.keys():
                setattr(item, key, values[key])
    item.save()
    return item


@transaction.atomic()
def bulk_create_items(model, values):
    if type(values) is not list:
        raise MyError(errors.API_INVALID_INPUT, 'ورودی شما باید به صورت لیست باشد')
    items = []
    for value in values:
        item = model()
        for key in vars(item):
            if not key.startswith('_') and not key == 'id':
                if key in value.keys():
                    setattr(item, key, value[key])
        item.save()
        items.append(item)
    return items


def update_item(old_item, new_item):
    for key in vars(old_item):
        if not key.startswith('_') and not key == 'id':
            if key in new_item.keys() and not new_item[key] == "":
                setattr(old_item, key, new_item[key])
    old_item.save()
    return old_item


@transaction.atomic
def bulk_update_items(model, new_items):
    if type(new_items) is not list:
        raise MyError(errors.API_INVALID_INPUT, 'ورودی شما باید به صورت لیست باشد')
    new_items = sorted(new_items, key=itemgetter('id'))
    ids = [item['id'] for item in new_items]
    old_items = model.objects.filter(pk__in=ids).order_by('id')
    for i in range(len(old_items)):
        for key in vars(old_items[i]):
            if not key.startswith('_') and not key == 'id':
                if key in new_items[i].keys() and not new_items[i][key] == "":
                    setattr(old_items[i], key, new_items[i][key])
        old_items[i].save()
    return old_items


def get_user_by_id(user_id):
    try:
        return MyUser.objects.get(pk=user_id)
    except models.ObjectDoesNotExist:
        raise MyError(errors.API_USER_NOT_FOUND)


def get_item_by_id(model, item_id):
    try:
        return model.objects.get(pk=item_id)
    except models.ObjectDoesNotExist:
        raise MyError(errors.API_ITEM_NOT_FOUND)
