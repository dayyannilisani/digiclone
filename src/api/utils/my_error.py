class MyError(Exception):
    def __init__(self, info, message=""):
        self.info = info
        if message != "":
            self.info["error"]["message"] = message
