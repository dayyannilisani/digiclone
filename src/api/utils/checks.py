from functools import wraps

from . import errors
from .my_error import MyError


def login(fn):
    @wraps(fn)
    def wrapper_function(self, request, *args, **kwargs):
        if not request.is_logged_in:
            raise MyError(errors.API_LOGIN_REQUIRED)
        return fn(self, request, *args, **kwargs)

    return wrapper_function


def admin(fn):
    @wraps(fn)
    def wrapper_function(self, request, *args, **kwargs):
        if not request.is_logged_in or not request.role == 'admin':
            raise MyError(errors.API_ADMIN_REQUIRED)
        return fn(self, request, *args, **kwargs)

    return wrapper_function


def valid(data):
    if not data.is_valid():
        message = ""
        for item in data.errors:
            print(data.errors)
            message = f'{data.errors[item][0]}\n{message}'
        raise MyError(errors.API_INVALID_INPUT, message)


