import ast
from django.core.cache import cache
from json import loads, dumps
from rest_framework import status
from rest_framework.response import Response


def delete(cache_key):
    def decorator(function):
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            cache.delete_pattern(cache_key)
            return result

        return wrapper

    return decorator


def check_and_response(cache_key):
    def decorator(function):
        def wrapper(*args, **kwargs):
            cached = cache.get(cache_key)
            if cached is not None:
                response = ast.literal_eval(cached)
                return Response(response, status=status.HTTP_200_OK)
            else:
                result = function(*args, **kwargs)
                cache.set(cache_key, str(loads(dumps(result.data))))
            return result

        return wrapper

    return decorator
