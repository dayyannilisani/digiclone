from rest_framework import serializers
from .models import Category, SubCategory


class CategoryOut(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class SubCategoryOut(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = '__all__'


class SubCategoryWithParentOut(serializers.ModelSerializer):
    category = CategoryOut()

    class Meta:
        model = SubCategory
        fields = '__all__'


class CategoryWithSubsOut(serializers.ModelSerializer):
    subCategories = SubCategoryOut(many=True)

    class Meta:
        model = Category
        fields = '__all__'
