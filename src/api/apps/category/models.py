from django.db import models

from ...utils.validation import generate_validation_message


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=200, blank=False, unique=True,
                             error_messages=generate_validation_message('عنوان'))

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Category, self).save(*args, **kwargs)

    class Meta:
        default_related_name = 'categories'
        db_table = 'categories'
        verbose_name = 'categorie'


class SubCategory(models.Model):
    title = models.CharField(max_length=200, blank=False, unique=True,
                             error_messages=generate_validation_message('عنوان'))

    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 error_messages=generate_validation_message('دسته بندی', ['invalid']))

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(SubCategory, self).save(*args, **kwargs)

    class Meta:
        default_related_name = 'subCategories'
        db_table = "subCategories"
        verbose_name = 'subCategorie'
