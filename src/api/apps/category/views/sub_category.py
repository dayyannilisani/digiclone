from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import SubCategory
from ..serializers import SubCategoryOut
from ....utils import checks,cache
from ....utils.services import bulk_create_items, bulk_update_items

sub_category_cache_name = 'sub_categories'


class SubCategoryPostPut(APIView):
    @checks.admin
    @cache.delete(sub_category_cache_name)
    def post(self, request):
        sub_categories = bulk_create_items(SubCategory, request.data)
        response = SubCategoryOut(sub_categories, many=True)
        return Response(response.data, status=status.HTTP_201_CREATED)

    @checks.admin
    @cache.delete(sub_category_cache_name)
    def put(self, request):
        categories = bulk_update_items(SubCategory, request.data)
        response = SubCategoryOut(categories, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class SubCategoryGetAndDelete(APIView):
    @cache.check_and_response(sub_category_cache_name)
    def get(self, request, subs: str):
        category = int(subs)
        categories = SubCategory.objects.filter(category=category)
        response = SubCategoryOut(categories, many=True)
        return Response(response.data, status=status.HTTP_200_OK)

    @checks.admin
    @transaction.atomic
    @cache.delete(sub_category_cache_name)
    def delete(self, request, subs: str):
        ids = subs.split(',')
        categories = SubCategory.objects.filter(pk__in=ids)
        for category in categories:
            category.delete()
        return Response(status=status.HTTP_200_OK)

