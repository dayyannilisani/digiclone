from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import Category
from ..serializers import CategoryOut, CategoryWithSubsOut
from ....utils import checks, cache
from ....utils.services import bulk_create_items, bulk_update_items

category_cache_name = 'categories'


class CategoryExceptDelete(APIView):
    @checks.admin
    @cache.delete(category_cache_name)
    def post(self, request):
        categories = bulk_create_items(Category, request.data)
        response = CategoryOut(categories, many=True)
        return Response(response.data, status=status.HTTP_201_CREATED)

    @checks.admin
    @cache.delete(category_cache_name)
    def put(self, request):
        categories = bulk_update_items(Category, request.data)
        response = CategoryOut(categories, many=True)
        return Response(response.data, status=status.HTTP_200_OK)

    @cache.check_and_response(category_cache_name)
    def get(self, request):
        categories = Category.objects.all().prefetch_related("subCategories")
        response = CategoryWithSubsOut(categories, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class CategoryDelete(APIView):
    @checks.admin
    @transaction.atomic
    @cache.delete(category_cache_name)
    def delete(self, request, categories: str):
        ids = categories.split(',')
        categories = Category.objects.filter(pk__in=ids)
        for category in categories:
            category.delete()

        return Response(status=status.HTTP_200_OK)
