from django.urls import path
from .views.category import CategoryExceptDelete, CategoryDelete
from .views.sub_category import SubCategoryPostPut, SubCategoryGetAndDelete

urlpatterns = [
    path('', CategoryExceptDelete.as_view()),
    path('bulk/<str:categories>/', CategoryDelete.as_view()),
    path('sub/', SubCategoryPostPut.as_view()),
    path('sub/<str:subs>/', SubCategoryGetAndDelete.as_view())
]
