from django.db import models
from django.utils.timezone import now
from ..user.models import MyUser
from ..product.models import Product

from ...utils.validation import generate_validation_message


# Create your models here.
class Order(models.Model):
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    status = models.CharField(max_length=20,
                              choices=[('paying', 'paying'), ('coming', 'coming'), ('received', 'received')],
                              default='paying',
                              error_messages=generate_validation_message('وضعیت'))
    created_at = models.DateTimeField(default=now)
