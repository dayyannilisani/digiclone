from rest_framework import statusupdate_item
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Order
from .serializers import OrderWithProductOut
from ...utils import checks, errors
from ...utils.my_error import MyError
from ...utils.services import create_item, get_item_by_id


# Create your views here.
class OrderCreate(APIView):
    @checks.login
    def post(self, request):
        request.data['status'] = 'paying'
        request.data['user_id'] = request.user_id
        create_item(Order, request.data)
        return Response(status=status.HTTP_201_CREATED)


class OrderDelete(APIView):
    @checks.login
    def delete(self, request, order_id):
        order = get_item_by_id(Order, order_id)
        if request.user_id != order.user_id and request.role != 'admin':
            raise MyError(errors.API_ADMIN_REQUIRED)
        order.delete()
        return Response(status=status.HTTP_200_OK)


class OrderGet(APIView):
    @checks.login
    def get(self, request):
        orders = Order.objects.filter(user_id=request.user_id).select_related('product')
        response = OrderWithProductOut(orders, many=True)
        return Response(response.data, status=status.HTTP_200_OK)
