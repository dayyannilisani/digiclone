from rest_framework import serializers
from .models import Order
from ..product.serializers import ProductOut


class OrderWithProductOut(serializers.ModelSerializer):
    product = ProductOut()

    class Meta:
        model = Order
        fields = '__all__'
