from django.urls import path

from .views.user import CreateUserView
from .views.token import TokenByNamePassView, TokenByRefreshView
from .views.admin import AdminGetUser, AdminChangeUser, AdminCreateUser

urlpatterns = [
    path("", CreateUserView.as_view()),
    path("token/", TokenByNamePassView.as_view()),
    path("token/refresh/", TokenByRefreshView.as_view()),
    path("admin/", AdminCreateUser.as_view()),
    path("admin/<int:user_id>/", AdminChangeUser.as_view()),
    path("admin/<int:offset>/<int:limit>/", AdminGetUser.as_view()),
]
