from django.db.models import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import MyUser
from ..serializers.inputs import TokenByNamePassInp, TokenByRefreshInp
from ....utils import checks, errors
from ....utils.my_error import MyError
from ....utils.services import get_user_by_id
from ..services.token import generate_tokens, validate_refresh_token


class TokenByNamePassView(APIView):
    def post(self, request):
        credentials = TokenByNamePassInp(data=request.data)
        checks.valid(credentials)
        try:
            user = MyUser.objects.get(name=credentials.validated_data['name'])
        except ObjectDoesNotExist:
            raise MyError(errors.API_INVALID_CREDENTIALS)
        if user is None or not user.check_password(credentials.validated_data['password']):
            raise MyError(errors.API_INVALID_CREDENTIALS)
        tokens = generate_tokens(user.id, user.role)
        return Response(tokens, status=status.HTTP_200_OK)


class TokenByRefreshView(APIView):
    def post(self, request):
        token = TokenByRefreshInp(data=request.data)
        checks.valid(token)
        user_id = validate_refresh_token(token.validated_data['refreshToken'])
        user = get_user_by_id(user_id)
        tokens = generate_tokens(user.id, user.role)
        return Response(tokens, status=status.HTTP_200_OK)
