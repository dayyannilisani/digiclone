from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import MyUser
from ..serializers.outputs import UserOut
from ..services.password import hash_password
from ....utils import checks
from ....utils.services import create_item, get_user_by_id, update_item


class AdminCreateUser(APIView):
    @checks.admin
    def post(self, request):
        request.data['password'] = hash_password(request.data)
        user = create_item(MyUser, request.data)
        result = UserOut(user)
        return Response(result.data, status=status.HTTP_201_CREATED)


class AdminChangeUser(APIView):
    @checks.admin
    def delete(self, request, user_id):
        user = get_user_by_id(user_id)
        user.delete()
        return Response(status=status.HTTP_200_OK)

    @checks.admin
    def put(self, request, user_id):
        request.data['password'] = hash_password(request.data)
        user = get_user_by_id(user_id)
        updated_user = update_item(user, request.data)
        response = UserOut(updated_user)
        return Response(response.data, status=status.HTTP_200_OK)


class AdminGetUser(APIView):
    @checks.admin
    def get(self, request, offset, limit):
        users = MyUser.objects.all()[offset:limit]
        response = UserOut(users, many=True)
        return Response(response.data, status=status.HTTP_200_OK)
