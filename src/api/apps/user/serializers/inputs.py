from rest_framework import serializers

from ....utils.validation import generate_validation_message


class TokenByNamePassInp(serializers.Serializer):
    name = serializers.CharField(max_length=150,
                                 error_messages=generate_validation_message('نام', ['null', 'required', 'blank']))
    password = serializers.CharField(max_length=500, error_messages=generate_validation_message('رمز عبور',
                                                                                                ['null', 'required',
                                                                                                 'blank']))


class TokenByRefreshInp(serializers.Serializer):
    token_required_message = "لطفا توکن خود را ارسال کنید"
    refreshToken = serializers.CharField(max_length=500, error_messages={
        'required': token_required_message,
        'blank': token_required_message,
        'null': token_required_message
    })
