from rest_framework import serializers
from ..models import MyUser


class UserOut(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        exclude = ['password']
