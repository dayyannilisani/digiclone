from django.db import models
from django.contrib.auth.hashers import check_password
from ...utils.validation import validate_phone, generate_validation_message


# Create your models here.
class MyUser(models.Model):
    name = models.CharField(max_length=150, unique=True, blank=False,
                            error_messages=generate_validation_message('نام'))
    email = models.EmailField(unique=True, blank=False, error_messages=generate_validation_message('ایمیل'))
    phone = models.CharField(max_length=11, unique=True, blank=False, validators=[validate_phone],
                             error_messages=generate_validation_message('شماره تلفن'))
    role = models.CharField(max_length=10, choices=[('admin', 'admin'), ('user', 'user')], default='user',
                            error_messages=generate_validation_message('نقش', ['invalid']))
    password = models.CharField(max_length=500, blank=False,
                                error_messages=generate_validation_message('رمزعبور', ['null', 'blank', 'invalid']))
    address = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.full_clean()
        super(MyUser, self).save(*args, **kwargs)

    def check_password(self, input_password):
        return check_password(input_password, self.password)


