from django.contrib.auth.hashers import make_password

from ....utils import my_error, errors


def hash_password(data):
    if 'password' not in data.keys() or data['password'] == '' or data['password'] is None:
        raise my_error.MyError(errors.API_INVALID_INPUT, "رمز عبور شما نمی تواند خالی باشد")

    return make_password(data['password'])
