import os
import jwt
import time

from ....utils import errors
from ....utils.my_error import MyError

access_secret = os.getenv("ACCESS_JWT_SECRET")
access_expiration = os.getenv("ACCESS_EXPIRATION")
refresh_secret = os.getenv("REFRESH_JWT_SECRET")
refresh_expiration = os.getenv("REFRESH_EXPIRATION")


def generate_tokens(user_id: int, role: str):
    now = int(time.time())
    refresh_token = jwt.encode({'id': user_id,
                                'exp': now + int(refresh_expiration),
                                'iat': now},
                               refresh_secret,
                               algorithm='HS256')
    access_token = jwt.encode({'id': user_id,
                               'role': role,
                               'exp': now + int(access_expiration),
                               'iat': now}, access_secret, algorithm='HS256')
    return {
        'refreshToken': refresh_token,
        'accessToken': access_token
    }


def validate_access_token(token: str):
    try:
        payload = jwt.decode(token, access_secret)
    except jwt.DecodeError:
        raise MyError(errors.API_INVALID_TOKEN)
    except jwt.ExpiredSignatureError:
        raise MyError(errors.API_EXPIRED_TOKEN)

    return {
        'id': payload['id'],
        'role': payload['role']
    }


def validate_refresh_token(token: str):
    try:
        payload = jwt.decode(token, refresh_secret)
    except jwt.DecodeError:
        raise MyError(errors.API_INVALID_TOKEN)
    except jwt.ExpiredSignatureError:
        raise MyError(errors.API_EXPIRED_TOKEN)

    return payload['id']
