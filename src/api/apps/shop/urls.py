from django.urls import path
from .views import ShopChange, ShopCreateAndGet

urlpatterns = [
    path('', ShopCreateAndGet.as_view()),
    path('<int:shop_id>/', ShopChange.as_view())
]
