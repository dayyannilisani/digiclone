from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = 'api.apps.shop'
