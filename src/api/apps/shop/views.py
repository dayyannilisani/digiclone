from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Shop
from .serializers import ShopOut
from ...utils import cache, checks
from ...utils.services import create_item, update_item, get_item_by_id

shop_cache_key = "shops"


# Create your views here.
class ShopCreateAndGet(APIView):
    @checks.admin
    @cache.delete(shop_cache_key)
    def post(self, request):
        shop = create_item(Shop, request.data)
        response = ShopOut(shop)
        return Response(response.data, status=status.HTTP_201_CREATED)

    @cache.check_and_response(shop_cache_key)
    def get(self, request):
        shops = Shop.objects.all()
        response = ShopOut(shops, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class ShopChange(APIView):
    @checks.admin
    @cache.delete(shop_cache_key)
    def delete(self, request, shop_id):
        shop = get_item_by_id(Shop, shop_id)
        shop.delete()
        return Response(status=status.HTTP_200_OK)

    @checks.admin
    @cache.delete(shop_cache_key)
    def put(self, request, shop_id):
        shop = get_item_by_id(Shop, shop_id)
        updated_shop = update_item(shop, request.data)
        response = ShopOut(updated_shop)
        return Response(response.data, status=status.HTTP_200_OK)
