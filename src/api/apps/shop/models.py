from django.db import models
from ...utils.validation import validate_phone, generate_validation_message


def pic_path(self, file):
    return f'{self.title}.jpg'


# Create your models here.
class Shop(models.Model):
    title = models.CharField(max_length=100, unique=True, blank=False, null=False,
                             error_messages=generate_validation_message('عنوان'))
    address = models.CharField(max_length=500, blank=False, null=False,
                               error_messages=generate_validation_message("ادرس"))
    phone = models.CharField(max_length=11, blank=False, null=False, validators=[validate_phone])
    lat = models.FloatField()
    lng = models.FloatField()
    logo = models.ImageField(upload_to=pic_path)

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Shop, self).save(*args, **kwargs)
