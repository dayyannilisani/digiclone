from django.contrib import admin
from .models import Product, ProductImage


# Register your models here.
@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    search_fields = ['title']


@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    search_fields = ['image']
