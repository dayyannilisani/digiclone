from django.urls import path
from .views.product import ProductCreate, ProductSpecificActions, ProductGet, ProductSearch
from .views.image import ImageCreate, ImageDelete

urlpatterns = [
    path('', ProductCreate.as_view()),
    path('search/<str:search_text>/', ProductSearch.as_view()),
    path('<int:product_id>/', ProductSpecificActions.as_view()),
    path('<int:offset>/<int:limit>/', ProductGet.as_view()),
    path('image/', ImageCreate.as_view()),
    path('image/<int:image_id>', ImageDelete.as_view())
]
