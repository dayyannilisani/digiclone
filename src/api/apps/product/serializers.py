from rest_framework import serializers
from .models import Product, ProductImage
from ..category.serializers import SubCategoryWithParentOut
from ..shop.serializers import ShopOut


class ProductOut(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductImageOut(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductDetailOut(serializers.ModelSerializer):
    categories = SubCategoryWithParentOut(many=True)
    productImages = ProductImageOut(many=True)
    shop = ShopOut()

    class Meta:
        model = Product
        fields = '__all__'
