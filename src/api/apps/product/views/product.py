from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db import models
from json import loads, dumps
from ..models import Product
from ..serializers import ProductOut, ProductDetailOut
from ....utils import checks, errors
from ....utils.my_error import MyError
from ....utils.services import create_item, update_item, get_item_by_id
from ....settings import es


class ProductCreate(APIView):
    @checks.admin
    def post(self, request):
        product = create_item(Product, request.data)
        categories = request.data['categories'].split(',')
        for cat in categories:
            product.categories.add(int(cat))
        response = ProductOut(product)
        es.index(index='digi', id=product.id, body=loads(dumps(response.data)))
        return Response(response.data, status=status.HTTP_201_CREATED)


class ProductSpecificActions(APIView):
    @checks.admin
    def delete(self, request, product_id):
        product = get_item_by_id(Product, product_id)
        product.delete()
        return Response(status=status.HTTP_200_OK)

    @checks.admin
    def put(self, request, product_id):
        product = get_item_by_id(Product, product_id)
        updated_product = update_item(product, request.data)
        categories = request.data['categories'].split(',')
        updated_product.categories.clear()
        for cat in categories:
            updated_product.categories.add(int(cat))
        response = ProductOut(updated_product)
        es.index(index='digi', id=updated_product.id, body=loads(dumps(response.data)))
        return Response(response.data, status=status.HTTP_200_OK)

    def get(self, request, product_id):
        try:
            product = Product.objects.select_related('shop').prefetch_related("productImages").get(pk=product_id)
        except models.ObjectDoesNotExist:
            raise MyError(errors.API_ITEM_NOT_FOUND)
        response = ProductDetailOut(product)
        return Response(response.data, status=status.HTTP_200_OK)


class ProductGet(APIView):
    def get(self, request, offset, limit):
        products = Product.objects.all()[offset, limit]
        response = ProductOut(products, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class ProductSearch(APIView):
    def get(self, request, search_text):
        products = es.search(body={
            "query": {
                "multi_match": {
                    "query": search_text,
                    "fields": ["description", "title"]
                }
            }
        })
        result = []
        for item in products['hits']['hits']:
            result.append(item['_source'])
        return Response(result, status=status.HTTP_200_OK)
