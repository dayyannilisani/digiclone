from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import ProductImage
from ....utils import checks
from ....utils.services import create_item, get_item_by_id


class ImageCreate(APIView):
    @checks.admin
    def post(self, request):
        image = create_item(ProductImage, request.data)
        return Response(status=status.HTTP_201_CREATED)


class ImageDelete(APIView):
    @checks.admin
    def delete(self, request, image_id):
        image = get_item_by_id(ProductImage, image_id)
        image.delete()
        return Response(status=status.HTTP_200_OK)
