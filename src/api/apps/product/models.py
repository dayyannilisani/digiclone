from django.db import models
from django.core.validators import MinValueValidator
from ..shop.models import Shop
from ..category.models import SubCategory
from ...utils.services import get_random_string
from ...utils.validation import generate_validation_message


def pic_path(self, file):
    return f'{self.title} {get_random_string(8)}.jpg'


# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=500, null=False, blank=False,
                             error_messages=generate_validation_message('عنوان'))
    description = models.TextField(null=True, blank=True)
    price = models.IntegerField(validators=[MinValueValidator(0)], error_messages=generate_validation_message('قیمت'))
    stock = models.IntegerField(validators=[MinValueValidator(0)], error_messages=generate_validation_message('موجودی'))
    weight = models.FloatField(validators=[MinValueValidator(0)], error_messages=generate_validation_message('وزن'))
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, error_messages=generate_validation_message('مغازه'))
    preview = models.ImageField(upload_to=pic_path, error_messages=generate_validation_message('پیش نمایش'))
    categories = models.ManyToManyField(SubCategory)

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Product, self).save(*args, **kwargs)


class ProductImage(models.Model):
    image = models.ImageField(upload_to=pic_path)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.image

    def __repr__(self):
        return self.image

    def save(self, *args, **kwargs):
        self.full_clean()
        super(ProductImage, self).save(*args, **kwargs)

    class Meta:
        default_related_name = 'productImages'
        db_table = "productImages"
        verbose_name = 'productImage'
