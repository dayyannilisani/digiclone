from rest_framework import serializers
from .models import Comment
from ..user.serializers.outputs import UserOut


class CommentsOfProductOut(serializers.ModelSerializer):
    user = UserOut()

    class Meta:
        model = Comment
        exclude = ['product']
