from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Comment
from .serializers import CommentsOfProductOut
from ...utils import checks, errors
from ...utils.my_error import MyError
from ...utils.services import create_item, update_item, get_item_by_id


class CommentCreate(APIView):
    @checks.login
    def post(self, request):
        request.data['user_id'] = request.user_id
        comment = create_item(Comment, request.data)
        return Response(status=status.HTTP_201_CREATED)


class CommentChange(APIView):
    @checks.login
    def delete(self, request, comment_id):
        comment = get_item_by_id(Comment, comment_id)
        if request.user_id != comment.user_id and request.role != 'admin':
            raise MyError(errors.API_ADMIN_REQUIRED)
        comment.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, comment_id):
        comment = get_item_by_id(Comment, comment_id)
        if request.role != 'admin' and request.user_id != comment.user_id:
            raise MyError(errors.API_ADMIN_REQUIRED)
        request.data['user_id'] = request.user_id
        update_item(comment, comment_id)
        return Response(status=status.HTTP_200_OK)


class CommentsOfProduct(APIView):
    def get(self, request, product_id, offset, limit):
        comments = Comment.objects.filter(product_id=product_id)[offset:limit]
        response = CommentsOfProductOut(comments, many=True)
        return Response(response.data, status=status.HTTP_200_OK)
