from django.urls import path
from .views import CommentChange, CommentCreate, CommentsOfProduct

urlpatterns = [
    path('', CommentCreate.as_view()),
    path('<int:comment_id>/', CommentChange.as_view()),
    path('<int:product_id>/<int:offset>/<int:limit>/', CommentsOfProduct.as_view())
]
