from django.db import models
from ..user.models import MyUser
from ..product.models import Product
from ...utils.validation import generate_validation_message


# Create your models here.
class Comment(models.Model):
    comment = models.CharField(max_length=1000, null=False, error_messages=generate_validation_message('نظر'))
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE, error_messages=generate_validation_message('کاربر'))
    product = models.ForeignKey(Product, on_delete=models.CASCADE, error_messages=generate_validation_message('کالا'))

    def __str__(self):
        return self.comment

    def __repr__(self):
        return self.comment
